var mongoose = require("mongoose");

var trashFileSchema = new mongoose.Schema({
	name: { 
		type: String, 
		required: true,
		unique: true
	},
	text: String,
	createdAt: { type: Number, required: true },
	_creator: { type: mongoose.Schema.Types.ObjectId, required: true }
});


module.exports = TrashFile = mongoose.model("trashfile", trashFileSchema);

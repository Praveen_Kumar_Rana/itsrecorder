const express = require("express");
var app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const db = require("./config/keys").mongoURI;
const passport = require("passport");

mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => {
    console.log("Mongoose connected");
  })
  .catch(err => console.log(err));

//body-parser
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());

//passport middleware
app.use(passport.initialize());

var publicDir = require("path").join(__dirname, "/public");
app.use(express.static(publicDir));
//routes
require("./routes/api/user")(app);
require("./routes/verifyaccount")(app);
require("./routes/api/file")(app);

app.get("/", (req, res) => {
  res.json({
    post: {
      "/user/register": "to register",
      "/user/login": "to login",
      "/file": "to upload file",
      "/verifyaccount/email": "to verify email using otp",
      "/verifyaccount/sms": "to verify sms otp",
      "/sendVerificationCode": "to reset password or resend verification code",
      "/reset/:secretToekn": "forgot password"
    },
    get: {
      "/current": "current user",
      "/file": "to fetch file"
    }
  });
});
//passport config
require("./config/passport")(passport);

const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log(`Server started at port ${port}`);
});

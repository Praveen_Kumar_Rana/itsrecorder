import React, { Component } from "react";
import Navbar from "./components/layout/Navbar";
import Register from "./components/Register";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import store from "./store";
import { Provider } from "react-redux";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";

import Login from "./components/Login";
import VerifyAccount from "./components/VerifyAccount";
import Dashboard from "./layouts/Dashboard/Dashboard.jsx";
import PrivateRoute from "./common/PrivateRoute";
import UserProfile from "views/UserProfile/UserProfile";
import ResetPassword from "./views/ResetPassword";
import Forgot from "./components/Forgot";
import EditFile from "./views/TableList/EditFile";
// Check for token
if (localStorage.jwtToken) {
  // Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());

    // Redirect to login
    window.location.href = "/login";
  }
}

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path="/" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/verifyaccount" component={VerifyAccount} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/forgot" component={Forgot} />
            <PrivateRoute exact path="/reset" component={ResetPassword} />
            <PrivateRoute path="/" component={Dashboard} />
            <PrivateRoute exact path="/user" component={UserProfile} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

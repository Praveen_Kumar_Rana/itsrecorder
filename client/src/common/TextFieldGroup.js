import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
function TextFieldGroup({
  name,
  placeholder,
  value,
  error,
  info,
  type,
  onChange,
  disabled,
  icons
}) {
  return (
    <div className="form-group">
      <div
        className={classnames("input-group ", {
          "has-error": error
        })}
      >
        <span className="input-group-addon">
          <i className={icons} />
        </span>
        <input
          type={type}
          className="form-control"
          placeholder={placeholder}
          name={name}
          value={value}
          onChange={onChange}
          disabled={disabled}
        />
      </div>
      {info && <small className="form-text text-muted">{info}</small>}
      {error && <div className="form-danger">{error}</div>}
    </div>
  );
}

TextFieldGroup.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  info: PropTypes.string,
  error: PropTypes.string,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.string
};

TextFieldGroup.defaultProps = {
  type: "text"
};

export default TextFieldGroup;

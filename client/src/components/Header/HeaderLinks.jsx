import React, { Component } from "react";
import { NavItem, Nav, NavDropdown, MenuItem } from "react-bootstrap";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import "./Header.css"
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
class HeaderLinks extends Component {
  onLogoutClick = e => {
    this.props.logoutUser();
  };

  onPClick = () => {
    this.props.history.push("/reset");
  };

  render() {
    const { isAuthenticated, user } = this.props.auth;
    const notification = (
      <div>
        <i className="fa fa-globe" />
        <b className="caret" />
        <span className="notification">5</span>
        <p className="hidden-lg hidden-md">Notification</p>
      </div>
    );
    return (
      <div>
        <Nav>
          <NavItem eventKey={1}  className="dashboard-nav-flex" href="#">
            <i className="fa fa-dashboard" />
            <p className="hidden-lg hidden-md">Recorder</p>
          </NavItem>
        </Nav>
        <Nav pullRight>
          <NavDropdown
            eventKey={2}
            title="Account"
            id="basic-nav-dropdown-right"
          >
            <MenuItem eventKey={2.1}>
              <Link to="/user">
                User Profile
              </Link>
            </MenuItem>
            <MenuItem eventKey={2.2} onClick={this.onPClick}>
              Change Password
            </MenuItem>
            <MenuItem eventKey={2.3} onClick={this.onLogoutClick}>
              Log Out
            </MenuItem>
          </NavDropdown>
          {/* <NavItem eventKey={3} onClick={this.onLogoutClick}>
            Log out
          </NavItem> */}
        </Nav>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(withRouter(HeaderLinks));

import React, { Component } from "react";
import "./VerifyAccount.css";
import { Link } from "react-router-dom";
import Proptypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import TextFieldGroup from "../common/TextFieldGroup";
import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";
class VerifyAccount extends Component {
  state = {
    secretToken: "",
    errors: {}
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    axios
      .post("/verifyaccount/sms", { secretToken: this.state.secretToken })
      .then(res => {
        if (res.data == "Verified") {
          alert("OTP Verified");
          this.props.history.push("/login");
        }
      })
      .catch(e => this.setState({ errors: e.response.data }));
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="register n ">
        <Navbar />
        <div class="login-form">
          <form noValidate onSubmit={this.onSubmit}>
            <h2 class="text-center" style={{ color: "black" }}>
              Verify
            </h2>

            <label style={{ color: "black" }}>Enter Message OTP</label>
            <TextFieldGroup
              placeholder="Message otp"
              name="secretToken"
              value={this.state.secretToken}
              onChange={this.onChange}
              error={errors.secretToken}
            />

            <div class="form-group">
              <button type="submit" class="btn btn-primary  btn-block">
                Verify
              </button>
            </div>
            <div class="clearfix">
              <Link to="/login" class="pull-left-inline">
                Already Verified?
              </Link>
            </div>
          </form>
          <p class="text-center text-muted small">
            Don't have an account? <Link to="/login">Register Here</Link>
          </p>
        </div>
        <Footer />
      </div>
    );
  }
}
VerifyAccount.propTypes = {
  errors: Proptypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.error
});

export default connect(mapStateToProps)(VerifyAccount);

import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import Proptypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../actions/authActions";
//import "./Register.css";
import TextFieldGroup from "../common/TextFieldGroup";
import Footer from "./layout/Footer";
import SideNavbar from "./SideNavbar/SideNavbar";
class Login extends Component {
  state = {
    username: "",

    password: "",
    errors: {}
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    const newUser = {
      username: this.state.username,
      password: this.state.password
    };
    this.props.loginUser(newUser);
  };

  render() {
    const { errors } = this.state;
    return (
      <Fragment>
      <SideNavbar/>
      <div className="register n">
        
        <div class="login-form">
          <form noValidate onSubmit={this.onSubmit}>
            <h2 class="text-center" style={{ color: "black" }}>
              Login
            </h2>
            <TextFieldGroup
              placeholder="Username"
              name="username"
              value={this.state.username}
              onChange={this.onChange}
              error={errors.username}
              icons="fa fa-user"
            />
            <TextFieldGroup
              placeholder="Password"
              name="password"
              type="password"
              value={this.state.password}
              onChange={this.onChange}
              error={errors.password}
              icons="fa fa-lock"
            />

            <div class="form-group">
              <button type="submit" class="btn btn-primary  btn-block">
                Sign in
              </button>
            </div>
            <div class="clearfix">
              <Link
                to="verifyaccount"
                class="pull-left-inline"
                style={{ margin: "5%" }}
              >
                Not Verified?
              </Link>
              <Link to="/forgot" class="center">
                Forgot Password?
              </Link>
            </div>
          </form>
          <p class="text-center">
            Don't have an account? <Link to="/register">Register Here</Link>
          </p>
        </div>
        <Footer />
      </div>
      </Fragment>
    );
  }
}
Login.propTypes = {
  loginUser: Proptypes.func.isRequired,
  auth: Proptypes.object.isRequired,
  errors: Proptypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.error
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Login);

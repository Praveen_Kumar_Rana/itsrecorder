import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Tabs, Tab } from "react-bootstrap";
import File from "components/File";
import FileView from "../../components/FileView";
import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import { Table, Grid, Row, Col, Button, Modal } from "react-bootstrap";

import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import axios from "axios";
import _ from "lodash";
import {
  EditorState,
  convertToRaw,
  convertFromHTML,
  ContentState
} from "draft-js";

function uploadImageCallBack(file) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest(); // eslint-disable-line no-undef
    xhr.open("POST", "https://api.imgur.com/3/image");
    xhr.setRequestHeader("Authorization", "Client-ID 8d26ccd12712fca");
    const data = new FormData(); // eslint-disable-line no-undef
    data.append("image", file);
    xhr.send(data);
    xhr.addEventListener("load", () => {
      const response = JSON.parse(xhr.responseText);
      resolve(response);
    });
    xhr.addEventListener("error", () => {
      const error = JSON.parse(xhr.responseText);
      reject(error);
    });
  });
}

const initialEditorState = () => {
  return EditorState.createWithContent(
    ContentState.createFromBlockArray(convertFromHTML("Write Here..."))
  );
};

export default class TableList extends Component {
  state = {
    files: [],
    selectedFileId: "",
    fileName: "",
    editorState: initialEditorState(),
    activeButton: "create",
    alertModelOpen: false,
    alertMessage: "",
    show: false
  };
  config = { headers: { Authorization: localStorage.getItem("jwtToken") } };

  componentWillMount() {
    axios.get("/file", this.config).then(res => {
      this.setState({ files: res.data.files });
    });
  }

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = () => {
    this.setState({ show: true });
  };
  onEditorStateChange = editorState => this.setState({ editorState });

  onSave = e => {
    e.preventDefault();
    const fileText = draftToHtml(
      convertToRaw(this.state.editorState.getCurrentContent())
    );
    const fileName = this.state.fileName;
    if (!fileName) {
      return alert("Give a name to your file!");
    }
    const fileId = this.state.selectedFileId;
    if (!fileId) {
      return axios
        .post(`/file`, { name: fileName, text: fileText }, this.config)
        .then(response => {
          const newFile = response.data.file;
          this.setState(prevState => ({
            files: [newFile, ...prevState.files],
            selectedFileId: newFile._id,
            fileName: newFile.name,
            alertModelOpen: true,
            alertType: "success",
            alertMessage: "File Saved Successfuly!"
          }));
        })
        .catch(e => {
          console.log(e);
        });
    }
  };
  onFileInputChange = e => {
    this.setState({ fileName: e.target.value });
  };

  render() {
    console.log(this.state);

    const onLinkClick = e => {
      //f.preventDefault();
      this.setState({ show: true, alertMessage: e.text });
    };
    const TableList = () => {
      return this.state.files.map(file => {
        return (
          <tr>
            <td>{file.name}</td>
            <td>
              <Link
                to="/"
                onClick={e => {
                  e.preventDefault();
                  onLinkClick(file);
                }}
              >
                View
              </Link>
            </td>
          </tr>
        );
      });
    };
    const columns = [
      {
        dataField: "name",
        text: "File Name"
      },
      {
        dataField: "text",
        text: "File Text"
      }
    ];
    return (
      <div>
        <Tabs defaultActiveKey="upload" id="uncontrolled-tab-example">
          <Tab eventKey="upload" title="Upload">
            <div>
              <Grid>
                <Row>
                  <Col md={11}>
                    <Card
                      title="Write File"
                      content={
                        <div>
                          <input
                            type="text"
                            placeholder="File Name"
                            onChange={this.onFileInputChange}
                          />
                          <button
                            className="btn btn-primary btn-sm"
                            onClick={this.onSave}
                            style={{ margin: "2px" }}
                          >
                            Save
                          </button>
                          <Editor
                            editorState={this.state.editorState}
                            toolbarClassName="toolbarClassName"
                            wrapperClassName="wrapperClassName"
                            editorClassName="editorClassName"
                            onEditorStateChange={this.onEditorStateChange}
                            toolbar={{
                              inline: { inDropdown: true },
                              list: { inDropdown: true },
                              textAlign: { inDropdown: true },
                              link: { inDropdown: true },
                              history: { inDropdown: true },
                              image: {
                                uploadCallback: uploadImageCallBack,
                                previewImage: true,
                                alt: { present: true }
                              }
                            }}
                          />
                        </div>
                      }
                    />
                  </Col>
                </Row>
              </Grid>
            </div>
          </Tab>
          <Tab eventKey="list" title="List">
            <div>
              <Modal show={this.state.show} onHide={this.handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>{this.state.alertMessage}</Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={this.handleClose}>
                    Close
                  </Button>
                  <Button variant="primary" onClick={this.handleClose}>
                    Save Changes
                  </Button>
                </Modal.Footer>
              </Modal>
              <Table responsive>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Table heading</th>
                  </tr>
                </thead>
                <tbody>
                  <TableList />
                </tbody>
              </Table>
            </div>
          </Tab>
        </Tabs>
      </div>
    );
  }
}

import React, { Component, Fragment } from "react";
import "./Register.css";
import TextFieldGroup from "../common/TextFieldGroup";
import Proptypes from "prop-types";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { registerUser } from "../actions/authActions";
import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";
import SideNavbar from "./SideNavbar/SideNavbar";
class Register extends Component {
  state = {
    firstName: "",
    lastName: "",
    username: "",
    state: "",
    city: "",
    mobile: "",
    email: "",
    password: "",
    password2: "",
    errors: {}
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    const newUser = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      username: this.state.username,
      state: this.state.state,
      city: this.state.city,
      mobile: this.state.mobile,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2
    };
    this.props.registerUser(newUser, this.props.history);
  };

  render() {
    const { errors } = this.state;
    return (
      <Fragment>
        <SideNavbar/>
        <div className="login-form">
          <form noValidate onSubmit={this.onSubmit}>
            <h2 className="text-center" style={{ color: "black" }}>
              Sign Up
            </h2>
            <TextFieldGroup
              placeholder="First Name"
              name="firstName"
              value={this.state.firstName}
              onChange={this.onChange}
              error={errors.firstName}
              icons="fa fa-user"
            />
            <TextFieldGroup
              placeholder="Last Name"
              name="lastName"
              value={this.state.lastName}
              onChange={this.onChange}
              error={errors.lastName}
              icons="fa fa-user"
            />
            <TextFieldGroup
              placeholder="Username"
              name="username"
              value={this.state.username}
              onChange={this.onChange}
              error={errors.username}
              icons="fa fa-user"
            />
            <TextFieldGroup
              placeholder="Email"
              name="email"
              type="email"
              value={this.state.email}
              onChange={this.onChange}
              error={errors.email}
              icons="fa fa-envelope"
            />
            <TextFieldGroup
              placeholder="Mobile"
              name="mobile"
              type="number"
              value={this.state.mobile}
              onChange={this.onChange}
              error={errors.mobile}
              icons="fa fa-mobile"
            />
            <TextFieldGroup
              placeholder="State"
              name="state"
              value={this.state.state}
              onChange={this.onChange}
              error={errors.state}
              icons="fa fa-user-md"
            />
            <TextFieldGroup
              placeholder="City"
              name="city"
              value={this.state.city}
              onChange={this.onChange}
              error={errors.city}
              icons="fa fa-user-md"
            />
            <TextFieldGroup
              placeholder="Password"
              name="password"
              type="password"
              value={this.state.password}
              onChange={this.onChange}
              error={errors.password}
              icons="fa fa-lock"
            />
            <TextFieldGroup
              placeholder="Confirm Password"
              name="password2"
              type="password"
              value={this.state.password2}
              onChange={this.onChange}
              error={errors.password2}
              icons="fa fa-lock"
            />
            <div className="form-group">
              <button type="submit" className="btn btn-primary  btn-block">
                Register
              </button>
            </div>
          </form>
          <p className="text-center">
            Already have an account? <Link to="/login">LogIn Here</Link>
          </p>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

Register.prototypes = {
  registerUser: Proptypes.func.isRequired,
  auth: Proptypes.object.isRequired,
  errors: Proptypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.error
});

export default connect(
  mapStateToProps,
  { registerUser }
)(withRouter(Register));

import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import "./SideNavbar.css";
import DesktopNavbar from '../DesktopNavbar/DesktopNavbar';

const SideNavbar = () => {
    return(
        <Fragment>
            <DesktopNavbar/>
        <div className="navbar-container">
            <div className="navbar-main">
                <div className="navbar-header">
                    <Link to="/" className="navbar-brand">Recorder</Link>
                    <p>Using stale data because the host is inaccessible</p>
                </div>
                <div className="bottom-nav-part">
                <ul className="nav navbar-nav">
                  <li className="active">
                    <Link to="/">Home</Link>
                  </li>

                  <li>
                    <Link to="/about">About</Link>
                  </li>
                  <li>
                    <Link to="/howtouse">How to Use</Link>
                  </li>
                </ul>
                    <ul className="nav navbar-nav navbar-right">
                        <li>
                            <Link to="/register">
                            <span className="glyphicon glyphicon-user" /> Sign Up
                            </Link>
                        </li>
                        <li>
                            <Link to="/login">
                            <span className="glyphicon glyphicon-log-in" /> Login
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        </Fragment>
    )
}

export default SideNavbar;
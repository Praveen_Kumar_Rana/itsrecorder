import React, { Component } from "react";
import { NavLink } from "react-router-dom";

import HeaderLinks from "../Header/HeaderLinks.jsx";

import imagine from "assets/img/sidebar-3.jpg";
import logo from "assets/img/reactlogo.png";

import dashboardRoutes from "routes/dashboard.jsx";

class Sidebar2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth
    };
  }

  updateDimensions() {
    this.setState({ width: window.innerWidth });
  }
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }
  render() {
    const sidebarBackground = {
      backgroundImage: "url(" + imagine + ")"
    };
    return (
      <div
        id="sidebar"
        className="sidebar"
        data-color="black"
        data-image={imagine}
      >
        <div className="sidebar-background" style={sidebarBackground} />
        <div className="logo">
          <a href="/" className="simple-text logo-mini">
            <div className="logo-img">
              <img src={logo} alt="logo_image" />
            </div>
          </a>
          <a href="/" className="simple-text logo-normal">
            Recorder
          </a>
        </div>
        <div className="sidebar-wrapper">
          <ul className="nav">
            {this.state.width <= 991 ? <HeaderLinks /> : null}
            <li key={0}>
              <NavLink
                to="/dashboard"
                className="nav-link"
                activeClassName="active"
              >
                <i className="pe-7s-graph" />
                <p>Dashboard</p>
              </NavLink>
            </li>

            <li key={1}>
              <NavLink to="/user" className="nav-link" activeClassName="active">
                <i className="pe-7s-user" />
                <p>User Profile</p>
              </NavLink>
            </li>
            <li key={2}>
              <NavLink to="/file" className="nav-link" activeClassName="active">
                <i className="pe-7s-note2" />
                <p>Create Files</p>
              </NavLink>
            </li>
            <li key={3}>
              <NavLink to="/file" className="nav-link" activeClassName="active">
                <i className="pe-7s-note2" />
                <p>View Files</p>
              </NavLink>
            </li>
            <li key={4}>
              <NavLink
                to="/about"
                className="nav-link"
                activeClassName="active"
              >
                <i className="pe-7s-news-paper" />
                <p>About</p>
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Sidebar2;

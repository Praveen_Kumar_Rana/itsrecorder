import React from 'react';
import {Link} from 'react-router-dom';
import "./DesktopNavbar.css";
import $ from 'jquery';

class DesktopNavbar extends React.Component{


    componentDidMount = () => {
        $(".display-menu").click(function(){
            $(".navbar-container").toggleClass("show-menu");
        })

        $(".navbar-container").click(function(){
            $(".navbar-container").toggleClass("show-menu");
        })
    }
    
    render(){
    return(
        <div className="top-navbar-container">
            <div className="top-navbar-main">
                <div className="top-navbar">
                    <Link to="/" className="navbar-brand">Recorder</Link>
                </div>
                <button className="display-menu btn btn-primary btn-primary-x">IIII</button>
                <div className="top-nav-part">
                <ul className="nav navbar-nav">
                  <li className="active">
                    <Link to="/" class="nav-item">Home</Link>
                  </li>

                  <li>
                    <Link to="/about">About</Link>
                  </li>
                  <li>
                    <Link to="/howtouse">How to Use</Link>
                  </li>
                </ul>
                    <ul className="nav navbar-nav navbar-right">
                        <li>
                            <Link to="/register">
                            <span className="glyphicon glyphicon-user" /> Sign Up
                            </Link>
                        </li>
                        <li>
                            <Link to="/login">
                            <span className="glyphicon glyphicon-log-in" /> Login
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
    }
}

export default DesktopNavbar;
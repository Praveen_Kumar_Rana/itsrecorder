import React, { Component } from "react";
import "./VerifyAccount.css";
import { Link } from "react-router-dom";
import Proptypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import TextFieldGroup from "../common/TextFieldGroup";
import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";
import _ from "lodash";
import SideNavbar from "./SideNavbar/SideNavbar";
class Forgot extends Component {
  state = {
    newPassword: "",
    newPassword1: "",
    mobile: "",
    secretToken: "",
    errors: {},
    isOTP: false,
    isVerified: false
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  sendOtp = e => {
    e.preventDefault();
    axios
      .post("/sendVerificationCode", { mobile: this.state.mobile })
      .then(res => {
        if (res.status === 200) this.setState({ isOTP: true });
      })
      .catch(e => this.setState({ errors: e.response.data }));
  };

  onVerify = e => {
    e.preventDefault();
    axios
      .post("/verifyaccount/sms", { secretToken: this.state.secretToken })
      .then(res => {
        if (res.data == "Verified") {
          alert("OTP Verified");
          this.setState({ isVerified: true });
        }
      })
      .catch(e => this.setState({ errors: e.response.data }));
  };

  onSubmit = e => {
    const data = _.pick(this.state, ["newPassword", "newPassword1", "mobile"]);
    e.preventDefault();
    axios
      .post("/forgot", data)
      .then(res => {
        if ((res.status = 200)) {
          alert("Password Changed Successfully!!!");
          this.props.history.push("/login");
        }
      })
      .catch(err => {
        console.log(2, err.response);
        this.setState({ errors: err.response.data.errors });
        console.log(this.state);
      });
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="register n ">
        <SideNavbar/>
        <div class="login-form">
          <form noValidate>
            <h2 class="text-center" style={{ color: "black" }}>
              Forgot
            </h2>
            <TextFieldGroup
              placeholder="Enter Mobile"
              name="mobile"
              value={this.state.mobile}
              onChange={this.onChange}
              error={errors.mobile}
              icons="fa fa-user"
            />
            <div class="form-group">
              <button class="btn btn-primary  btn-block" onClick={this.sendOtp}>
                Send Otp
              </button>
            </div>
            {this.state.isOTP && (
              <label style={{ color: "black" }}>Enter OTP</label>
            )}
            {this.state.isOTP && (
              <TextFieldGroup
                placeholder="Message otp"
                name="secretToken"
                value={this.state.secretToken}
                onChange={this.onChange}
                error={errors.secretToken}
              />
            )}
            {this.state.isOTP && (
              <div class="form-group">
                <button
                  class="btn btn-primary  btn-block"
                  onClick={this.onVerify}
                >
                  Verify
                </button>
              </div>
            )}
            {this.state.isVerified && (
              <TextFieldGroup
                placeholder="Enter Password"
                name="newPassword"
                type="password"
                value={this.state.newPassword}
                onChange={this.onChange}
                error={errors.newPassword}
                icons="fa fa-lock"
              />
            )}
            {this.state.isVerified && (
              <TextFieldGroup
                placeholder="Confirm Password"
                name="newPassword1"
                type="password"
                value={this.state.newPassword1}
                onChange={this.onChange}
                error={errors.newPassword1}
                icons="fa fa-lock"
              />
            )}
            {this.state.isVerified && (
              <div class="form-group">
                <button
                  class="btn btn-primary  btn-block"
                  onClick={this.onSubmit}
                >
                  Reset
                </button>
              </div>
            )}
          </form>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Forgot;

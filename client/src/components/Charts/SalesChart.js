import React from 'react';
import ReactChartist from 'react-chartist';
import axios from 'axios';

const dataStock = {
  labels: ['\'07','\'08','\'09', '\'10', '\'11', '\'12', '\'13', '\'14', '\'15'],
  series: [
    [22.20, 34.90, 42.28, 51.93, 62.21, 80.23, 62.21, 82.12, 102.50, 107.23]
  ]
};

const optionsStock = {
  lineSmooth: false,
  height: "260px",
  axisY: {
    offset: 40,
    labelInterpolationFnc: function(value) {
        return '$' + value;
      }

  },
  low: 10,
  high: 110,
    classNames: {
      point: 'ct-point ct-green',
      line: 'ct-line ct-green'
  }
};

class SalesChart extends React.Component{
	constructor(props){
		super(props);
		this.state={
			presentfiles:[],
			deletedfiles:[],
			row:[],
			row1:[]
		}
	}

	componentWillMount() {
		axios.get("/file", this.config).then(res => {
			this.setState({ presentfiles: res.data.files });
			var rowArray = [];
			for(var i=1;i<=this.state.presentfiles.length; i++){
				
				if(Boolean(this.state.presentfiles) === true){
					// console.log("The value of i is : "+i);
					// console.log(this.state.presentfiles[i-1].name);
					// converting the epoch time to IST
					var d = new Date(this.state.presentfiles[i-1].createdAt);
					var year = d.getUTCFullYear();
					var mm = d.getMonth();
					var dd = d.getDate();
					rowArray[i-1]={
						id: i,
						name: this.state.presentfiles[i-1].name,
						date: dd+"/"+mm+"/"+year,
					}
				}
				
				
			}
			// console.log("Printing the data from the this.state.row");
			this.setState({
				row:rowArray
			})
			// console.log(this.state.row);
		});
	
		axios.get("/deletedfile", this.config).then(res => {
            this.setState({ deletedfiles: res.data.files });
			var rowArray1 = [];
			for(var i=1;i<=this.state.deletedfiles.length; i++){
				
				if(Boolean(this.state.deletedfiles) === true){
					// console.log("The value of i is : "+i);
					// console.log(this.state.deletedfiles[i-1].name);
					// converting the epoch time to IST
					var d = new Date(this.state.deletedfiles[i-1].createdAt);
					var year = d.getUTCFullYear();
					var mm = d.getMonth();
					var dd = d.getDate();
					rowArray1[i-1]={
						id: i,
						name: this.state.deletedfiles[i-1].name,
						date: dd+"/"+mm+"/"+year,
					}
				}
			}
			// console.log("Printing the data from the this.state.row from the trash file.");
			this.setState({
				row1:rowArray1
			})
			// console.log(this.state.row);
		});
		if(this.state.row && this.state.row1){
			console.log('The file list');
			console.log(this.state.row);
			console.log(this.state.row1);
		}
    }

	handleChange = e => {
		e.preventDefault();
		if(this.state.row && this.state.row1){
			console.log('The file list');
			console.log(this.state.row);
			console.log(this.state.row1);
			
			console.log("Printing the arranged array!");
			var arr = [];
			var pointer = 0;
			for(var i=0; i<this.state.row.length; i++){
				if(pointer===0){
					arr.push([this.state.row[i].date,1]);
					pointer++;
				} else {
					var done = 0;
					for(var j=0; j<arr.length; j++){
						if(arr[j][0]==this.state.row[i].date){
							arr[j][1] = arr[j][1]+1;
							done = 1;
							break;
						}
					}
					if(done==0){
						arr.push([this.state.row[i].date,1]);
					}
				}
			}
			console.log(arr);
			var arr1 = [];
			var pointer1 = 0;
			for(var i=0; i<this.state.row1.length; i++){
				if(pointer1===0){
					arr1.push([this.state.row1[i].date,1]);
					pointer1++;
				} else {
					var done = 0;
					for(var j=0; j<arr1.length; j++){
						if(arr1[j][0]==this.state.row1[i].date){
							arr1[j][1] = arr1[j][1]+1;
							done = 1;
							break;
						}
					}
					if(done==0){
						arr1.push([this.state.row1[i].date,1]);
					}
				}
			}
			console.log(arr1);
			var date = new Date();
			var year = date.getFullYear();
			var dd = date.getDate();
			var mm = date.getMonth();
			var todayDate = dd+"/"+mm+"/"+year;
			console.log("Todays date : "+todayDate);
		} else {
			console.log("Generating the data.");
		}
	}


	render(){
		return(
			<div className="card">
				<div className="header">
					<h4>SalesChart: AAPL</h4>
					<p className="category">Line Chart with Points</p>
				</div>
				<div className="content">
					<ReactChartist data={dataStock} options={optionsStock} type="Line" className="ct-chart" />
				</div>
				<button class="btn btn-success"  onClick={this.handleChange}>Click to get the Data</button>
			</div>
		)
	}
}
export default SalesChart;
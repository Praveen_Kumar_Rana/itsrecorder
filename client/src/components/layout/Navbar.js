import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import "./Navbar.css";
class Navbar extends Component {
  onLogoutClick(e) {
    e.preventDefault();
    this.props.logoutUser();
  }

  render() {
    return (
      <Fragment>
      <div className="n">
        <nav className="navbar navbar-active">
          <div className="container">
            <div className="container-fluid">
              <div className="navbar-header">
                <Link className="navbar-brand" to="/">
                  Recorder
                </Link>
              </div>
              <div className="" id="">
                <ul className="nav navbar-nav">
                  <li className="active">
                    <Link to="/">Home</Link>
                  </li>

                  <li>
                    <Link to="/about">about</Link>
                  </li>
                  <li>
                    <Link to="/howtouse">How to Use</Link>
                  </li>
                </ul>
                <ul className="nav navbar-nav navbar-right">
                  <li>
                    <Link to="/register">
                      <span className="glyphicon glyphicon-user" /> Sign Up
                    </Link>
                  </li>
                  <li>
                    <Link to="/login">
                      <span className="glyphicon glyphicon-log-in" /> Login
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Navbar);

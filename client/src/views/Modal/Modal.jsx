import React, {Component} from 'react';
import './Modal.css';

const Modal = ({show, Delete,id, hideModal,message }) => {
    return (
        (show && (
            <div className="modal-main">
            <div className="modal-box">
                <p>{message}</p>
                <button className="btn btn-primary" onClick={hideModal}>Cancel</button>
                <button className="btn btn-danger" id={id} onClick={Delete}>Delete</button>
            </div>
        </div>
        ))
    )
}

export default Modal;
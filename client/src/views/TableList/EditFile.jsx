// Required later
import React, { Component } from "react";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import {Redirect} from 'react-router-dom';
import {
  EditorState,
  convertToRaw,
  convertFromHTML,
  ContentState
} from "draft-js";
import axios from "axios";
import _ from "lodash";

import { Dropdown } from "react-bootstrap";

function uploadImageCallBack(file) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest(); // eslint-disable-line no-undef
    xhr.open("POST", "https://api.imgur.com/3/image");
    xhr.setRequestHeader("Authorization", "Client-ID 8d26ccd12712fca");
    const data = new FormData(); // eslint-disable-line no-undef
    data.append("image", file);
    xhr.send(data);
    xhr.addEventListener("load", () => {
      const response = JSON.parse(xhr.responseText);
      resolve(response);
    });
    xhr.addEventListener("error", () => {
      const error = JSON.parse(xhr.responseText);
      reject(error);
    });
  });
}

const initialEditorState = () => {
  return EditorState.createWithContent(
    ContentState.createFromBlockArray(convertFromHTML("Write Here..."))
  );
};

class EditFile extends Component {
  state = {
    files: [],
    selectedFileId: "",
    fileName: "",
    editorState: initialEditorState(),
    activeButton: "create",
    alertModelOpen: false,
    alertMessage: "",
    show: false
  };
  config = { headers: { Authorization: localStorage.getItem("jwtToken") } };

  onEditorStateChange = editorState => this.setState({ editorState });

  onSave = e => {
    e.preventDefault();
    console.log("Save");
    console.log(this.state);
    const fileText = draftToHtml(
      convertToRaw(this.state.editorState.getCurrentContent())
    );
    const fileName = this.state.fileName;
    if (!fileName) {
      return alert("Give a name to your file!");
    }
    const fileId = this.state.selectedFileId;

    return axios
      .post(`/file`, { name: fileName, text: fileText }, this.config)
      .then(response => {
        if (response.status == 200) {
          alert("File Saved Successfully");
        }
        const newFile = response.data.file;
        this.setState(prevState => ({
          files: [newFile, ...prevState.files],
          selectedFileId: newFile._id,
          fileName: "",
          editorState: initialEditorState(),
          alertModelOpen: true,
          alertType: "success",
          alertMessage: "File Saved Successfuly!"
        }));
      })
      .catch(e => {
        console.log(e);
      });
  };

  onFileNameChange = e => {
    this.setState({ fileName: e.target.value });
  };

  onSelectChange = e => {
    let value = e.target.value;
    if (!value) {
      return;
    }
    const selectedFile = this.props.files[0];

    const fileName = selectedFile.name;
    const fileText = selectedFile.text;
    let rawFileText;
    if (convertFromHTML(fileText).contentBlocks) {
      rawFileText = convertFromHTML(fileText);
    } else {
      rawFileText = convertFromHTML("<p>Write...</p>");
    }
    const editorState = EditorState.createWithContent(
      ContentState.createFromBlockArray(rawFileText)
    );
    this.setState({
      selectedFileId: value,
      editorState,
      fileName
    });
  };

  render() {
    const SelectList = () => {
      return this.state.files.map(file => {
        return <option>{file.name}</option>;
      });
    };
    return (
      <div style={{padding:"20px"}}>
        <form>
          <div class="form-group">
            <select onChange={this.onSelectChange}>
              <option>Select Your Files-></option>
              <SelectList />
            </select>
          </div>
          <div class="form-group">
            <input
              type="text"
              placeholder="File Name"
              value={this.state.fileName}
              onChange={this.onFileNameChange}
            />
          </div>
          <button
            class="btn btn-default"
            onClick={this.onSave}
            style={{ margin: "2px" }}
          >
            Save
          </button>
        </form>
        <Editor
          editorState={this.state.editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={this.onEditorStateChange}
          toolbar={{
            inline: { inDropdown: true },
            list: { inDropdown: true },
            textAlign: { inDropdown: true },
            link: { inDropdown: true },
            history: { inDropdown: true },
            image: {
              uploadCallback: uploadImageCallBack,
              previewImage: true,
              alt: { present: true }
            }
          }}
        />
      </div>
    );
  }
}

export default EditFile;

import React, { Component, Fragment } from "react";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import jsPdf from 'jspdf';
import { MDBDataTable , MDBBtn} from 'mdbreact';

import {
	EditorState,
	convertToRaw,
	convertFromHTML,
	ContentState
} from "draft-js";
import axios from "axios";
import _ from "lodash";
import './ViewTableList.css'
import Modal from "../Modal/Modal";

const initialEditorState = () => {
return EditorState.createWithContent(
	ContentState.createFromBlockArray(convertFromHTML("Write Here..."))
	);
};

class Trash extends Component {
	state = {
		files: [],
		selectedFileId: "",
		fileName: "",
		editorState: initialEditorState(),
		activeButton: "create",
		alertModelOpen: false,
		alertMessage: "",
		show: false,
		readOnly:"readOnly",
		buttonText:"Edit",
        row:[],
        show:false,
        id:""
	};
	config = { headers: { Authorization: localStorage.getItem("jwtToken") } };

	onEditorStateChange = editorState => this.setState({ editorState });

	componentWillMount() {
		axios.get("/deletedfile", this.config).then(res => {
            this.setState({ files: res.data.files });
            console.log("Printing from the Deleted Files.");
            console.log(this.state.files);
			var rowArray = [];
			for(var i=1;i<=this.state.files.length; i++){
				
				if(Boolean(this.state.files) === true){
					console.log("The value of i is : "+i);
					console.log(this.state.files[i-1].name);
					// converting the epoch time to IST
					var d = new Date(this.state.files[i-1].createdAt);
					var year = d.getUTCFullYear();
					var mm = d.getMonth();
					var dd = d.getDate();
					rowArray[i-1]={
						id: i,
						name: this.state.files[i-1].name,
						date: dd+"/"+mm+"/"+year,
						download:<MDBBtn id={`dow${i}`} className="btn btn-primary" onClick={this.Download} size="sm">Download</MDBBtn>,
						view:<MDBBtn id={`vie${i}`} className="btn btn-success" size="sm" onClick={this.View}>View</MDBBtn>,
						// eidt:<MDBBtn id={`edi${i}`} className="btn btn-info" size="sm">Edit</MDBBtn>,
						delete: <MDBBtn id={`del${i}`} className="btn btn-danger" size="sm" onClick={this.showModal}>Delete</MDBBtn>,
					}
				}
				
				
			}
			console.log("Printing the data from the this.state.row from the trash file.");
			this.setState({
				row:rowArray
			})
			console.log(this.state.row);
		});
    }
    
    showModal = e => {
        e.preventDefault();
        console.log("Printing from the showModal.");
        console.log(e.target.id);
        this.setState({
            show: true,
            id:e.target.id
        })
        console.log(this.state.id);
    }

    hideModal = e => {
        e.preventDefault();
        this.setState({
            show: false,
            id:""
        })
    }

	Download = e => {
		e.preventDefault();
		console.log("Printing on Download button click.");
		console.log(e.target.id);
		const id = e.target.id[3];
		if(e.target.id.slice(0,3)=='dow'){
		console.log(this.state.files[id-1]);
		var doc = new jsPdf();
		doc.fromHTML(this.state.files[id-1].text, 1, 1);
		doc.save(this.state.files[id-1].name + ".pdf");
		}
	}

	View = e => {
		e.preventDefault();
		if(e.target.id.slice(0,3)=='vie'){
			const id = e.target.id[3];
			console.log(this.state.files[id-1]);
			var doc = new jsPdf();
			doc.fromHTML(this.state.files[id-1].text, 1, 1);
			doc.output("dataurlnewwindow");
		}
		
	}

	Delete = e => {
		e.preventDefault();
		const id = e.target.id[3];  
		console.log(this.state.files[id-1]);
		axios
		.post(`/trash/delete`, { name: this.state.files[id-1].name, text: this.state.files[id-1].text }, this.config)
		.then(response => {
			console.log(response);
			alert("File successfully deleted");
			window.location.reload();
		})
		.catch(e => {
			console.log(e);
		});
	}


  render() {

		const data = {
			columns: [
			{
				label: 'S. No.',
				field: 'sno',
				sort: 'asc',
				width: 50
				},
				{
					label: 'File Name',
					field: 'name',
					sort: 'asc',
					width: 270
				},
				{
					label: 'Date Updated',
					field: 'position',
					sort: 'asc',
					width: 100
				},
				{
					label: 'Download',
					field: 'download',
					width: 100
				},
				{
					label: 'View File',
					field: 'view',
					width: 100
				},
				// {
				// 	label: 'Edit',
				// 	field: 'edit',
				// 	width: 100
				// },
				{
					label: 'Delete File',
					field: 'delete',
					width: 100
				}
			],
		rows: this.state.row
		}

    return (
        <Fragment>
            <div style={{padding:"20px"}}>
                <MDBDataTable
                    striped
                    bordered
                    hover
                    data={data}
                />
            </div>
            <Modal Delete={this.Delete} id={this.state.id} hideModal={this.hideModal} show={this.state.show} message="This will be deleting the File Permamently"/>
        </Fragment>
    );
  }
}

export default Trash;

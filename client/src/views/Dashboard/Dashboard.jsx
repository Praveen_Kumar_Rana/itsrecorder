import React, { Component, Fragment } from "react";
import SalesChart from "../../components/Charts/SalesChart";

export default class Dashboard extends Component {
	render() {
		return (
			<Fragment>
				{/* <div>This portion starts from the Views/Dashboard/Dashboard.jsx</div> */}
				<SalesChart/>
			</Fragment>
		)
	}
}

import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import { UserCard } from "components/UserCard/UserCard.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import avatar from "assets/img/faces/face-3.jpg";
import Axios from "axios";
import _ from "lodash";
class UserProfile extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    mobile: "",
    city: "",
    state: "",
    username: "",
    selectedFile: null,
    createdAt: null,
    url: "",
    name: "",
    fb: "",
    google: "",
    twitter: ""
  };

  onChange = e => {
    this.setState({ selectedFile: e.target.files[0] });
  };

  config = { headers: { Authorization: localStorage.getItem("jwtToken") } };

  uploadHandler = e => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("myImage", this.state.selectedFile);
    const config1 = {
      headers: {
        "content-type": "multipart/form-data",
        Authorization: localStorage.getItem("jwtToken")
      }
    };
    Axios.post("/upload", formData, config1)
      .then(response => {
        this.setState({ name: response.data.filename });
        console.log(response.data);
      })
      .catch(error => {});
  };
  componentDidMount() {
    Axios.get("/current", this.config).then(res => {
      console.log(res);
      const body = _.pick(res.data, [
        "firstName",
        "lastName",
        "email",
        "mobile",
        "city",
        "state",
        "username"
      ]);
      console.log(res);
      body.name = res.name;
      body.fb = res.data.profile.fb || "";
      body.google = res.data.profile.google || "";
      body.twitter = res.data.profile.twitter || "";
      this.setState(body);
    });
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSubmit = e => {
    e.preventDefault();
    Axios.put("/user", this.state, this.config)
      .then(res => {
        if (res.status === 200) alert("Profile Details Updated Successfully!");
      })
      .catch(err => {
        console.log(err.response);
      });
  };
  render() {
    console.log(this.state);
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={8}>
              <Card
                title="Edit Profile"
                content={
                  <form onSubmit={this.handleSubmit}>
                    <FormInputs
                      ncols={["col-md-5", "col-md-3", "col-md-4"]}
                      proprieties={[
                        {
                          label: "Email",
                          type: "email",
                          bsClass: "form-control",
                          placeholder: "Email",
                          defaultValue: this.state.email,
                          disabled: true,
                          name: "email",
                          required: true
                        },
                        {
                          label: "Username",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Username",
                          defaultValue: this.state.username,
                          disabled: true,
                          name: "username",
                          required: true
                        },
                        {
                          label: "Mobile",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Mobile",
                          defaultValue: this.state.mobile,
                          onChange: this.handleChange,
                          name: "mobile",
                          required: true
                        }
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-6", "col-md-6"]}
                      proprieties={[
                        {
                          label: "First name",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "First name",
                          defaultValue: this.state.firstName,
                          onChange: this.handleChange,
                          name: "firstName",
                          required: true
                        },
                        {
                          label: "Last name",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Last name",
                          defaultValue: this.state.lastName,
                          onChange: this.handleChange,
                          name: "lastName",
                          required: true
                        }
                      ]}
                    />

                    <FormInputs
                      ncols={["col-md-6", "col-md-6"]}
                      proprieties={[
                        {
                          label: "City",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "City",
                          defaultValue: this.state.city,
                          onChange: this.handleChange,
                          name: "city",
                          required: true
                        },
                        {
                          label: "State",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "State",
                          defaultValue: this.state.state,
                          onChange: this.handleChange,
                          name: "state",
                          required: true
                        }
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-12"]}
                      proprieties={[
                        {
                          label: "Facebook",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Facebook Url",
                          defaultValue: this.state.fb,
                          onChange: this.handleChange,
                          name: "fb"
                        }
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-12"]}
                      proprieties={[
                        {
                          label: "Twitter",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Twitter Url",
                          defaultValue: this.state.twitter,
                          onChange: this.handleChange,
                          name: "twitter"
                        }
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-12"]}
                      proprieties={[
                        {
                          label: "Google",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Google Plus Url",
                          defaultValue: this.state.google,
                          onChange: this.handleChange,
                          name: "google"
                        }
                      ]}
                    />
                    <Button bsStyle="info" pullRight fill type="submit">
                      Update Profile
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
            <Col md={4}>
              <UserCard
                bgImage="https://images.unsplash.com/photo-1466854076813-4aa9ac0fc347?ixlib=rb-1.2.1&auto=format&fit=crop&w=1189&q=80"
                avatar={`/uploads/${this.state.name}`}
                name={this.state.firstName + " " + this.state.lastName}
                userName={this.state.username}
                description={
                  <span>
                    From{" "}
                    <h3>
                      {this.state.state}, {this.state.city}
                    </h3>
                  </span>
                }
                socials={
                  <div>
                    <a
                      target="__blank"
                      href={this.state.fb}
                      style={{ padding: "5px" }}
                    >
                      <i className="fa fa-facebook-square" />
                    </a>
                    <a href={this.state.twitter} style={{ padding: "5px" }}>
                      <i className="fa fa-twitter" />
                    </a>
                    <a
                      target="__blank"
                      href="twitter/com"
                      style={{ padding: "5px" }}
                    >
                      <i className="fa fa-google-plus-square" />
                    </a>
                  </div>
                }
              />
              <h4>Upload your profile picture:</h4>
              <input type="file" name="myImage" onChange={this.onChange} />
              <button onClick={this.uploadHandler}>Upload!</button>
            </Col>
          </Row>
        </Grid>
        >
      </div>
    );
  }
}

export default UserProfile;

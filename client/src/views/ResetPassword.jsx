import React, { Component } from "react";
import "components/VerifyAccount.css";
import Sidebar2 from "components/Sidebar/Sidebar2.jsx";
import { connect } from "react-redux";
import { logoutUser } from "../actions/authActions";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import TextFieldGroup from "common/TextFieldGroup";
import axios from "axios";
import _ from "lodash";

class ResetPassword extends Component {
  initialstate = {
    errors: {},
    password: "",
    newPassword: "",
    newPassword1: ""
  };

  state = this.initialstate;

  config = { headers: { Authorization: localStorage.getItem("jwtToken") } };
  onLogoutClick = e => {
    this.props.logoutUser();
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    const data = _.pick(this.state, [
      "password",
      "newPassword",
      "newPassword1"
    ]);
    e.preventDefault();
    axios
      .post("/reset", data, this.config)
      .then(res => {
        if ((res.status = 200)) {
          alert("Password Changed Successfully!!!");
          this.setState(this.initialstate);
        }
      })
      .catch(err => {
        console.log(2, err.response);
        this.setState({ errors: err.response.data.errors });
        console.log(this.state);
      });
  };
  render() {
    const { errors } = this.state;
    return (
      <div>
        <Sidebar2 />
        <nav className="navbar navbar-default password">
          <div>
            <div className="navbar-header">
              <Link className="navbar-brand" to="/">
                Recorder
              </Link>
            </div>
            <ul className="nav navbar-nav">
              <li className="active">
                <Link to="/dashboard">Dashboard</Link>
              </li>
              <li>
                <Link to="/user">Profile</Link>
              </li>
              <li>
                <Link onClick={this.onLogoutClick} to="/">
                  Log Out
                </Link>
              </li>
            </ul>
          </div>
        </nav>
        <div className="password">
          <div class="login-form">
            <form noValidate onSubmit={this.onSubmit}>
              <h2 class="text-center" style={{ color: "black" }}>
                Change Password
              </h2>

              <TextFieldGroup
                placeholder="Old Password"
                name="password"
                type="password"
                value={this.state.password}
                onChange={this.onChange}
                error={errors.password}
                icons="fa fa-lock"
              />
              <TextFieldGroup
                placeholder="New Password"
                name="newPassword"
                type="password"
                value={this.state.newPassword}
                onChange={this.onChange}
                error={errors.newPassword}
                icons="fa fa-lock"
              />
              <TextFieldGroup
                placeholder="Confirm new  Password"
                name="newPassword1"
                type="password"
                value={this.state.newPassword1}
                onChange={this.onChange}
                error={errors.newPassword1}
                icons="fa fa-lock"
              />

              <div class="form-group">
                <button type="submit" class="btn btn-primary  btn-block">
                  Change Password
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(withRouter(ResetPassword));

import Dashboard from "views/Dashboard/Dashboard";
import UserProfile from "views/UserProfile/UserProfile";
import TableList from "views/TableList/TableList";
import Typography from "views/Typography/Typography";
import Icons from "views/Icons/Icons";
import Maps from "views/Maps/Maps";
import Notifications from "views/Notifications/Notifications";
import Upgrade from "views/Upgrade/Upgrade";
import ViewTableList from "../views/TableList/ViewTableList";
import Trash from "../views/TableList/Trash";
import EditFile from "../views/TableList/EditFile";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard
  },
  {
    path: "/user",
    name: "User Profile",
    icon: "pe-7s-user",
    component: UserProfile
  },
  {
    path: "/file",
    name: "Create Files",
    icon: "pe-7s-note2",
    component: TableList
  },
  {
    path: "/viewfile",
    name: "View Files",
    icon: "fas fa-folder-open",
    component: ViewTableList
  },
  {
    path: "/trash",
    name: "Trash",
    icon: "fas fa-trash-alt",
    component: Trash
  },
  {
    path: "/typography",
    name: "About",
    icon: "pe-7s-news-paper",
    component: Typography
  },

  { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
];

export default dashboardRoutes;

const User = require("../../models/User");
const bcrypt = require("bcryptjs");
const _ = require("lodash");
const { sendVerifyMail } = require("../../misc/sendgrid");
const axios = require("axios");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const secret = require("../../config/keys").secretKey;
//Local Input validation
const validateRegisterInput = require("../validation/register");
const validateLoginInput = require("../validation/login");
const validateresetpassword = require("../validation/resetPassword");
const validateforgotpassword = require("../validation/forgotPassword");
const multer = require("multer");
const path = require("path");
const mkdirp = require("mkdirp");
const moment = require("moment");
// Set The Storage Engine
const storage = multer.diskStorage({
  destination: "./public/uploads/",
  filename: function(req, file, cb) {
    cb(null, "IMAGE-" + Date.now() + path.extname(file.originalname));
  }
});
// Init Upload
const upload = multer({
  storage: storage,
  limits: { fileSize: 1000000 },
  fileFilter: function(req, file, cb) {
    checkFileType(file, cb);
  }
}).single("myImage");

// Check File Type
function checkFileType(file, cb) {
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb("Error: Images Only!");
  }
}

module.exports = app => {
  //user register

  app.post("/user/register", (req, res) => {
    //console.log(req.body);
    const { errors, isValid } = validateRegisterInput(req.body);
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      User.findOne({
        $or: [
          { mobile: req.body.mobile },
          { email: req.body.email },
          { username: req.body.username }
        ]
      }).then(user => {
        if (user) {
          if (user.username === req.body.username)
            return res
              .status(400)
              .json({ username: "username already exists" });
          if (user.mobile === req.body.mobile)
            return res.status(400).json({ mobile: "mobile already exists" });
          if (user.email === req.body.email)
            return res.status(400).json({ email: "email already exists" });
        }
        var body = _.pick(req.body, [
          "firstName",
          "lastName",
          "email",
          "mobile",
          "password",
          "state",
          "city",
          "username"
        ]);

        body.secretEmailToken = Math.floor(Math.random() * 100000);
        body.secretMsgToken = Math.floor(Math.random() * 100000);
        const newUser = new User(body);
        axios
          .get(
            `http://jumbosms.shlrtechnosoft.com/websms/sendsms.aspx?userid=mayankmodi&password=mayank@051&sender=MANGLA&mobileno=${
              body.mobile
            }&msg=Your verification code is: ${
              body.secretMsgToken
            } to {req.headers.host}/verifyaccount/sms`
          )
          .then(res => console.log(res.data));
        const to = body.email;
        const text = `copy and paste the following code: ${
          body.secretEmailToken
        } to {req.headers.host}/verifyaccount/email`;
        const subject = "Verify your account on dictator";
        sendVerifyMail(to, subject, text);
        bcrypt.genSalt(10, function(err, salt) {
          bcrypt.hash(newUser.password, salt, function(err, hash) {
            // Store hash in your password DB.
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then(user => {
                res.json(user);
              })
              .catch(err => console.log(err));
          });
        });
      });
    }
  });

  //user login
  app.post("/user/login", (req, res) => {
    const { errors, isValid } = validateLoginInput(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      const username = req.body.username;
      const password = req.body.password;

      User.findOne({
        $or: [{ email: req.body.username }, { username: req.body.username }]
      }).then(user => {
        if (!user) return res.status(404).json({ username: "User not found" });
        if (!user.activeMobile) {
          return res
            .status(400)
            .send({ accountActive: false, username: "Account not active" });
        }
        bcrypt.compare(password, user.password).then(response => {
          if (!response)
            return res.status(404).json({ password: "password incorrect" });
          //user matched

          const payload = {
            id: user.id,
            firstName: user.firstName
          }; //create jwt payload

          //sign tocken
          jwt.sign(payload, secret, { expiresIn: 3600 }, (err, token) => {
            if (err) throw err;
            user.update({
              $set: {
                token: "Bearer " + token
              }
            });
            res.json({
              success: true,
              token: "Bearer " + token
            });
          });
        });
      });
    }
  });

  //reset password
  app.post(
    "/reset",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      const { errors, isValid } = validateresetpassword(req.body);

      // Check Validation
      if (!isValid) {
        return res.status(400).json({ errors, success: false });
      }
      User.findOne({ _id: req.user.id })
        .then(user => {
          if (!user) {
            return res
              .status(404)
              .json({ errors: "No User found", subject: false });
          }
          const password = req.body.password;
          const newPassword = req.body.newPassword;
          bcrypt.compare(password, user.password).then(response => {
            if (!response) {
              errors.password = "password incorrect";
              return res.status(400).json({ errors, success: false });
            }
            bcrypt.genSalt(10, function(err, salt) {
              bcrypt.hash(newPassword, salt, function(err, hash) {
                // Store hash in your password DB.
                if (err) throw err;
                user
                  .update({
                    $set: {
                      password: hash
                    }
                  })
                  .then(() =>
                    res.status(200).json({
                      success: true
                    })
                  )
                  .catch(err => res.status(400).json({ err, success: false }));
              });
            });
          });
        })
        .catch(e => {
          console.log(e);
          res.status(400).send({ e, success: false });
        });
    }
  );

  //forgot password
  app.post("/forgot", (req, res) => {
    const { errors, isValid } = validateforgotpassword(req.body);
    const { mobile } = req.body;
    // Check Validation
    if (!isValid) {
      return res.status(400).json({ errors, success: false });
    }
    User.findOne({ mobile })
      .then(user => {
        if (!user) {
          return res
            .status(404)
            .json({ errors: "No User found", subject: false });
        }
        const newPassword = req.body.newPassword;
        bcrypt.genSalt(10, function(err, salt) {
          bcrypt.hash(newPassword, salt, function(err, hash) {
            // Store hash in your password DB.
            if (err) throw err;
            user
              .update({
                $set: {
                  password: hash
                }
              })
              .then(() =>
                res.status(200).json({
                  success: true
                })
              )
              .catch(err => res.status(400).json({ err, success: false }));
          });
        });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send({ e, success: false });
      });
  });

  //current user
  app.get(
    "/current",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      res.json(req.user);
    }
  );

  //upload profile picture
  app.post(
    "/upload",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      upload(req, res, err => {
        console.log("Request ---", req.body);
        console.log("Request file ---", req.file);
        if (err) {
          console.log("err", err);
          res.send(err);
        } else {
          if (req.file == undefined) {
            console.log("undefined");
            res.send("error");
          } else {
            //console.log(req.file);

            User.findOne({ _id: req.user.id })
              .then(user => {
                if (!user) {
                  return res.status(404).send();
                }
                user.profile.name = req.file.filename;
                user.profile.url = req.file.destination;
                user.profile.createdAt = moment().valueOf();
                return user.save();
              })
              .then(() => {
                res.status(200).send(req.file);
              });
          }
        }
      });
    }
  );

  //update profile details

  app.put(
    "/user",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      //console.log(req.body);
      var body = _.pick(req.body, [
        "firstName",
        "lastName",
        "email",
        "mobile",
        "password",
        "state",
        "city",
        "username"
      ]);
      User.findById(req.user.id)
        .then(user => {
          user.firstName = req.body.firstName;
          user.lastName = req.body.lastName;
          user.mobile = req.body.mobile;
          user.state = req.body.state;
          user.city = req.body.city;
          user.profile.fb = req.body.fb;
          user.profile.twitter = req.body.twitter;
          user.profile.google = req.body.google;
          return user.save();
        })
        .then(() => {
          res.status(200).send({ success: true });
        });
    }
  );
};

const _ = require("lodash");
var moment = require("moment");
const File = require("../../models/File");
const TrashFile = require("../../models/Trash")
const validateFileInput = require("../validation/file");
const { authenticate } = require("../../middleware/authenticate");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const db = require("../../config/keys").mongoURI;
module.exports = app => {
	app.post(
		"/file",
		passport.authenticate("jwt", { session: false }),
		(req, res) => {
		const { errors, isValid } = validateFileInput(req.body);
		if (!isValid) {
			return res.status(400).json(errors);
		}
		const body = _.pick(req.body, ["name", "text"]);

		File.findOne({ name: body.name }).then(file => {
			if (file) {
			file.name = body.name+"1";
			file.text = body.text;
			file.createdAt = moment().valueOf();
			file
				.save()
				.then(file => {
				res.send({ file });
				})
				.catch(e => {
				console.log(e);
				res.status(400).send();
				});
			} else {
			body._creator = req.user.id;
			body.createdAt = moment().valueOf();
			//body._client = _client;
			new File(body)
				.save()
				.then(file => {
				res.send({ file });
				})
				.catch(e => {
				console.log(e);
				res.status(400).send();
				});
			}
		});
		}
	);

	// Editing the files
	app.post(
		"/edit",
		passport.authenticate("jwt", { session: false }),
		(req, res) => {
		const { errors, isValid } = validateFileInput(req.body);
		if (!isValid) {
			return res.status(400).json(errors);
		}
		const body = _.pick(req.body, ["name", "text"]);

		File.findOne({ name: body.name }).then(file => {
			if (file) {
			file.name = body.name;
			file.text = body.text;
			file.createdAt = moment().valueOf();
			file
				.save()
				.then(file => {
				res.send({ file });
				})
				.catch(e => {
				console.log(e);
				res.status(400).send();
				});
			} else {
				console.log("Error in sending the file.")
				res.status(400).send();
			}
		});
		}
	);

	//get case files of a client
	app.get(
		"/file",
		passport.authenticate("jwt", { session: false }),
		(req, res) => {
		const _creator = req.user._creator ? req.user._creator : req.user._id;
		File.find({ _creator })
			.then(files => {
			console.log(files);
			res.send({ files });
			})
			.catch(e => {
			res.status(400).send();
			});
		}
	);

	app.get(
		"/data",
		passport.authenticate("jwt", { session: false }),
		(req, res) => {
		const _creator = req.user._creator ? req.user._creator : req.user._id;
		File.find({ _creator })
			.then(files => {
			console.log(files);
			getData();
			res.send({ files });
			})
			.catch(e => {
			res.status(400).send();
			});
		}
	)

	//get particular file for editing
	app.get(
		"/edit",
		passport.authenticate("jwt", { session: false }),
		(req, res) => {
		const _creator = req.user._creator ? req.user._creator : req.user._id;
		File.find({ _creator })
			.then(files => {
			console.log(files);
			res.send({ files });
			})
			.catch(e => {
			res.status(400).send();
			});
		}
	);

	// delete the file to trash.
	app.post(
		"/delete",
		passport.authenticate("jwt", { session: false }),
		(req, res) => {
		const { errors, isValid } = validateFileInput(req.body);
		if (!isValid) {
			return res.status(400).json(errors);ß
		}
		const body = _.pick(req.body, ["name", "text"]);
		console.log(req.body);
		File.remove({ name: body.name }).then(file => {
			if (file) {
			console.log("Printing the content of the Removed file.");
			console.log(body);
			body._creator = req.user.id;
			body.createdAt = moment().valueOf();
			new TrashFile(body)
				.save()
				.then(file => {
				res.send({ file });
				})
				.catch(e => {
				console.log(e);
				res.status(400).send();
				});
		} else {
			res.status(400).send();
		}
		}
	)
	});

	//get the deleted case files of a client
	app.get(
		"/deletedfile",
		passport.authenticate("jwt", { session: false }),
		(req, res) => {
		console.log("Printing from the inside of the deletedFile Routes.")
		const _creator = req.user._creator ? req.user._creator : req.user._id;
		TrashFile.find({ _creator })
			.then(files => {
			console.log(files);
			res.send({ files });
			})
			.catch(e => {
			res.status(400).send();
			});
		}
	);

	// Delete the files from the trash - Permament deleting
	app.post(
		"/trash/delete",
		passport.authenticate("jwt", { session: false }),
		(req, res) => {
		const { errors, isValid } = validateFileInput(req.body);
		if (!isValid) {
			return res.status(400).json(errors);
		}
		const body = _.pick(req.body, ["name", "text"]);
		console.log(req.body);
		TrashFile.remove({ name: body.name }).then(file => {
				res.send({ file });
				})
				.catch(e => {
				console.log(e);
				res.status(400).send();
				});
		}
	)

	// Getting the count data from the databases.
	// app.get("/getcountdata",passport.authenticate("jwt", { session: false }),
	// 	(req, res) => {
	// 		var dataSet = {
	// 			present:[],
	// 			deleted:[]
	// 		}
		
	// 		try{
	// 			const countFilesPresent = db.getCollection('files').aggregate({$group:{_id:{$toDate: new Date(12-12-2019)}, count:{$sum:1}}})
	// 				console.log("Printing the count of the files from the get");
	// 				console.log(countFilesPresent);
	// 				dataSet.present = countFilesPresent;
	// 			const countFilesDeleted = db.getCollection('trashfiles').aggregate({$group:{_id:{$toDate: new Date(12-12-2019)}, count:{$sum:1}}})
	// 				console.log("Printing the count of the files from the get");
	// 				console.log(countFilesDeleted);
	// 				dataSet.deleted = countFilesDeleted;
	// 				return res.status(200).json({dataSet});
	// 		} catch(e){
	// 			console.log("Problem generated.");
	// 			return res.status(404).json(e);
	// 		}
	// 		console.log("Printing the DataSet.");
	// 		console.log(dataSet);
	// 	}
	// )

	app.get(
		"/getcountdata",
		passport.authenticate("jwt", { session: false }),
		(req, res) => {
		
			db.getCollection('files').aggregate({$group:{_id:{$toDate: new Date(12-12-2019)}, count:{$sum:1}}})
			.then(files => {
			console.log(files);
			res.send({ files });
			})
			.catch(e => {
			res.status(400).send();
			});
		}
	);
}

// const getData = async function(req, res, next){
// 	var dataSet = {
// 		present:[],
// 		deleted:[]
// 	}

// 	try{
// 		const countFilesPresent = await db.getCollection('files').aggregate({$group:{_id: "$lastModified", count:{$sum:1}}})
// 			console.log("Printing the count of the files from the get");
// 			console.log(countFilesPresent);
// 			dataSet.present = countFilesPresent;
// 		const countFilesDeleted = await db.getCollection('files').aggregate({$group:{_id: "$lastModified", count:{$sum:1}}})
// 			console.log("Printing the count of the files from the get");
// 			console.log(countFilesDeleted);
// 			dataSet.deleted = countFilesDeleted;
// 	} catch(e){
// 		console.log("Problem generated.");
// 	}
// 	console.log("Printing the DataSet.");
// 	console.log(dataSet);
// }

// db.getCollection("files").aggregate([
// 	{ "$group": {
//         "_id": {
//             "$dateToString": {
//                 "format": "%Y-%m-%d",
//                 "date": {
//                     "$add": [
//                         new Date(0), 
//                         { "$multiply": [1000, "$createdAt"] }
//                     ]
//                 }
//             }
//         },
//         "count": { "$sum": 1 }
//     } }
// ])


// { $dateToString: {
//     date: <dateExpression>,
//     format: <formatString>,
//     timezone: <tzExpression>,
//     onNull: <expression>
// } }

// db.getCollection('files').aggregate({$group:{_id:{$toDate: "$createdAt"}, count:{$sum:1}}})
const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function validateFileInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.text = !isEmpty(data.text) ? data.text : "";

  if (Validator.isEmpty(data.name)) {
    errors.name = " Name field is required";
  }

  if (Validator.isEmpty(data.text)) {
    errors.text = "text field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};

const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function validateresetpassword(data) {
  let errors = {};

  data.mobile = !isEmpty(data.mobile) ? data.mobile : "";
  data.newPassword = !isEmpty(data.newPassword) ? data.newPassword : "";
  data.newPassword1 = !isEmpty(data.newPassword1) ? data.newPassword1 : "";

  if (Validator.isEmpty(data.mobile)) {
    errors.mobile = "mobile field is required";
  }

  if (Validator.isEmpty(data.newPassword)) {
    errors.newPassword = "New Password field is required";
  }

  if (!Validator.isLength(data.newPassword, { min: 6, max: 30 })) {
    errors.newPassword = "Password must be at least 6 characters";
  }

  if (Validator.isEmpty(data.newPassword1)) {
    errors.newPassword1 = "Confirm Password field is required";
  } else {
    if (!Validator.equals(data.newPassword, data.newPassword1)) {
      errors.newPassword1 = "Passwords must match";
    }
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};

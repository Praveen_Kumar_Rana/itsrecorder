const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function validateverifyaccount(data) {
  let errors = {};

  data.secretToken = !isEmpty(data.secretToken) ? data.secretToken : "";

  if (Validator.isEmpty(data.secretToken)) {
    errors.secretToken = " Otp field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};

const User = require("../models/User");
const axios = require("axios");
//Local Input validation
const validateverifyaccount = require("./validation/verifyaccount");
const validatesendverification = require("./validation/sendverification");

module.exports = app => {
  //verify account by sending an email with randomString
  app.post("/verifyaccount/email", (req, res) => {
    const { errors, isValid } = validateverifyaccount(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }
    const secretEmailToken = req.body.secretToken.trim();
    User.findOne({ secretEmailToken })
      .then(user => {
        if (!user) {
          return res.status(404).send();
        }

        return user.update({
          $set: {
            secretEmailToken: "",
            activeMail: true
          }
        });
      })
      .then(() => {
        res.status(200).send("Verified");
      })
      .catch(e => {
        res.status(400).send();
      });
  });

  app.post("/verifyaccount/sms", (req, res) => {
    const { errors, isValid } = validateverifyaccount(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }
    const secretMsgToken = req.body.secretToken.trim();
    User.findOne({ secretMsgToken })
      .then(user => {
        if (!user) {
          return res.status(404).send({ secretToken: "Not found" });
        }

        return user.update({
          $set: {
            secretMsgToken: "",
            activeMobile: true
          }
        });
      })
      .then(() => {
        res.status(200).send("Verified");
      })
      .catch(e => {
        res.status(400).send();
      });
  });

  //forgot password || resend code
  app.post("/sendVerificationCode", (req, res) => {
    const { errors, isValid } = validatesendverification(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json({ errors, success: false });
    }
    const { mobile } = req.body;
    console.log(mobile);
    User.findOne({ mobile })
      .then(user => {
        if (!user) {
          return res
            .status(404)
            .send({ errors: "no user found", success: false });
        }
        const secretToken = Math.floor(Math.random() * 100000);

        subject = "reset password on dictator";
        text =
          "You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n" +
          "Please click on the following link, or paste this into your browser to complete the process:\n\n" +
          req.headers.host +
          "/reset/" +
          secretToken +
          "\n\n" +
          "If you did not request this, please ignore this email and your password will remain unchanged.\n";

        axios
          .get(
            `http://jumbosms.shlrtechnosoft.com/websms/sendsms.aspx?userid=mayankmodi&password=mayank@051&sender=MANGLA&mobileno=${mobile}&msg=Your verification code is: ${secretToken} to ${
              req.headers.host
            }/verifyaccount/sms`
          )
          .then(res => console.log(res.data, 1));

        //sendVerifyMail(to, subject, text);
        return user.update({
          $set: {
            secretMsgToken: secretToken,
            secretEmailToken: secretToken
          }
        });
      })
      .then(() => {
        res.status(200).send({ success: true });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send({ errors: e, success: false });
      });
  });
};
